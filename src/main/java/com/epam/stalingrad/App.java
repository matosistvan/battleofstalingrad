package com.epam.stalingrad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.stalingrad.domain.Battle;

public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(final String[] args) {

        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");

        Battle battle = applicationContext.getBean("battle", Battle.class);
        
        LOGGER.info(battle.play());
        LOGGER.info("Battle ended!");
        applicationContext.close();

    }

}
