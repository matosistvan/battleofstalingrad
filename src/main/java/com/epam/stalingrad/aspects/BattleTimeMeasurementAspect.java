package com.epam.stalingrad.aspects;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BattleTimeMeasurementAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(BattleTimeMeasurementAspect.class);

    @Around("battlePlays()")
    public Object measurePlayTime(final ProceedingJoinPoint joinPoint) throws Throwable {

        long startedNanoTime = System.nanoTime();

        LOGGER.info("BATTLE STARTED: " + new Date());

        Object result = joinPoint.proceed();

        LOGGER.info("BATTLE ENDED: " + new Date());
        LOGGER.info("BATTLE LASTED (sec): " + (System.nanoTime() - startedNanoTime) / 1000000000.0);

        return result;

    }

    @Pointcut("execution (String com.epam.stalingrad.domain.Battle.play())")
    private void battlePlays() {

    }

}