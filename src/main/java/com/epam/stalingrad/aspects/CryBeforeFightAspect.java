package com.epam.stalingrad.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.stalingrad.domain.LoudNess;
import com.epam.stalingrad.domain.Person;

@Aspect
@Component
public class CryBeforeFightAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(CryBeforeFightAspect.class);

    @Before("execution (String com.epam.stalingrad.domain.Person.fight()) && this(targetPerson)")
    public void cryBefore(final Person targetPerson) {
        LOGGER.info(targetPerson.getName() + ": " + targetPerson.battleCry(LoudNess.getRandom()));

    }

}
