package com.epam.stalingrad.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.epam.stalingrad.domain.LoudNess;
import com.epam.stalingrad.domain.Person;

@Aspect
@Component
public class MakeItLoudAspect {

    @Around(
            value = "execution (String com.epam.stalingrad.domain.Person.battleCry(com.epam.stalingrad.domain.LoudNess)) "
            		+ "&& args(loudNess) && target(targetPerson)",
            argNames = "targetPerson,loudNess")
    public String makeItLoud(final ProceedingJoinPoint joinPoint, final Person targetPerson, final LoudNess loudNess) throws Throwable {

        String result = (String) joinPoint.proceed();

        if (loudNess == LoudNess.HIGH) {
            result = result.toUpperCase();
        }

        return result;

    }

}