package com.epam.stalingrad.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
//@Component
public class PointcutTesterAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(PointcutTesterAspect.class);

    @Before("testPointcut()")
    public void logBefore(final JoinPoint joinPoint) {

        String targetObjectClassName = joinPoint.getTarget().getClass().getSimpleName();

        LOGGER.info(targetObjectClassName + " matched! - " + System.identityHashCode(joinPoint.getTarget()));

    }

    @Pointcut("execution (* com.epam.stalingrad.domain.Weapon.shoot(..))")
    private void testPointcut1() {
    }

    @Pointcut("execution (* use(..)) && within(com.epam.stalingrad.domain.*)")
    private void testPointcut2() {
    }

    @Pointcut("execution (* use(..)) && target(com.epam.stalingrad.domain.Weapon)")
    private void testPointcut3() {
    }

    @Pointcut("execution (* use(..)) && this(com.epam.stalingrad.domain.Weapon)")
    private void testPointcut4() {
    }

    @Pointcut("@target(com.epam.stalingrad.annotations.Blessed) && execution (* fight(..))")
    private void testPointcut5() {
    }

    @Pointcut("@within(com.epam.stalingrad.annotations.Blessed) && execution (* fight(..))")
    private void testPointcut6() {
    }


    @Pointcut("execution (* setDamage(..)) && args(int)")
    private void testPointcut9() {
    }

    @Pointcut("execution (* com.epam..*.setDamage(..))")
    private void testPointcut10() {
    }

}
