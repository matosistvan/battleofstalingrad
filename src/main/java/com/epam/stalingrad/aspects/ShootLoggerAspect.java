package com.epam.stalingrad.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.stalingrad.domain.Weapon;

@Aspect
@Component
public class ShootLoggerAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShootLoggerAspect.class);

    @Before("weaponShooting()")
    public void logShooting(final JoinPoint joinPoint) {

        String proxyObjectClassName = joinPoint.getThis().getClass().getName();
        LOGGER.info("Proxy object type: " + proxyObjectClassName);

        LOGGER.info("Original " + (joinPoint.getTarget().getClass().getName()));
        LOGGER.info("Is Weapon: " + (joinPoint.getThis() instanceof Weapon));

        LOGGER.info(joinPoint.getTarget().getClass().getSimpleName() + " is shooting!");
    }

    @Pointcut("execution (* com.epam.stalingrad.domain.Weapon.shoot(..))")
    private void weaponShooting() {
    }
}