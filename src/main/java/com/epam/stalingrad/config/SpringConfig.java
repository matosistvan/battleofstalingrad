package com.epam.stalingrad.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.stalingrad.domain.MachineGun;
import com.epam.stalingrad.domain.Pistol;
import com.epam.stalingrad.domain.RandomWeaponFactory;
import com.epam.stalingrad.domain.Rifle;

@Configuration
public class SpringConfig {

    @Autowired
    ApplicationContext context;

    @Bean
    public RandomWeaponFactory randomIonBlasterSimpleFactory() {
        return new RandomWeaponFactory() {

            @Override
            public Rifle getRifle() {
                return context.getBean(Rifle.class);
            }

            @Override
            public Pistol getPistol() {
                return context.getBean(Pistol.class);
            }

            @Override
            public MachineGun getMachineGun() {
                return context.getBean(MachineGun.class);
            }

        };
    }
}
