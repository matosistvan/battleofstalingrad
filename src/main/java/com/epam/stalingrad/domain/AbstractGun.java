package com.epam.stalingrad.domain;

public abstract class AbstractGun implements Weapon {

    private int damage;
    private final String serialNumber;

    public AbstractGun(final int damage) {
        this.damage = damage;
        long serialNumber = Math.round(Math.random() * 100000);
        this.serialNumber = String.valueOf(serialNumber);
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(final int damage) {
        this.damage = damage;
    }

    @Override
    public int getUniqueId() {
        return System.identityHashCode(this);
    }

    @Override
    public abstract String shoot();

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(" serialNumber=").append(serialNumber);
        return builder.toString();
    }

}
