package com.epam.stalingrad.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;

public class Army implements BeanNameAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(Army.class);

    private Map<FighterType, List<Person>> fighters = new HashMap<>();

    private String name;

    public Army(final Map<FighterType, List<Person>> fighters) {
        super();
        this.fighters = fighters;
    }

    public Map<FighterType, List<Person>> getFighters() {
        return fighters;
    }

    @PostConstruct
    public void sayReady() {
        LOGGER.info(name + " is ready to fight!");
    }

    @Override
    public void setBeanName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Fighters:");
        builder.append("\n");

        for (FighterType fighterType : fighters.keySet()) {
            builder.append(fighterType);

            List<Person> fightersInType = fighters.get(fighterType);
            builder.append("(");
            builder.append(fightersInType.size());
            builder.append(")");
            builder.append(": \n");

            for (Person person : fightersInType) {
                builder.append(person);
                builder.append("\n");
            }

            builder.append("\n");
        }

        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (fighters == null ? 0 : fighters.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Army other = (Army) obj;
        if (fighters == null) {
            if (other.fighters != null) {
                return false;
            }
        } else if (!fighters.equals(other.fighters)) {
            return false;
        }
        return true;
    }

}
