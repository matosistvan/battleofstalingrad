package com.epam.stalingrad.domain;

public interface Battle {

    String play();

}
