package com.epam.stalingrad.domain;

import java.util.List;
import java.util.Set;

public class BattleOnGround implements Battle {

    private final Battleground battleground;

    private final Set<Army> armies;

    public BattleOnGround(final Battleground battleground, final Set<Army> armies) {
        super();
        this.battleground = battleground;
        this.armies = armies;
    }

    public Battleground getBattleground() {
        return battleground;
    }

    public Set<Army> getArmies() {
        return armies;
    }

    @Override
    public String play() {

        StringBuilder result = new StringBuilder(toString());

        if (armies.size() == 2) {

            for (Army army : armies) {

                List<Person> attackers = army.getFighters().get(FighterType.ATTACKER);

                for (Person attacker : attackers) {
                    result.append(attacker.getName() + " says: " + attacker.fight());
                    result.append("\n");
                }

                result.append("\n");

                List<Person> defenders = army.getFighters().get(FighterType.DEFENDER);

                for (Person defender : defenders) {
                    result.append(defender.getName() + " says: " + defender.fight());
                    result.append("\n");
                }

                result.append("\n");

            }

        } else {
            result.append("There must be exactly 2 armies!");
        }

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            // do nothing
        }
        return result.toString();

    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Battle begins!\n");
        result.append("Battleground: " + battleground);
        result.append("\n");
        result.append("Armies:");
        result.append("\n\n");

        int i = 1;
        for (Army army : armies) {
            result.append("Army ");
            result.append(i);
            result.append(" :\n");
            result.append(army);
            result.append("\n\n");

            i++;
        }

        return result.toString();
    }

}
