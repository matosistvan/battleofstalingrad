package com.epam.stalingrad.domain;

public enum BattleSide {

    RED_ARMY, THIRD_EMPIRE;

	public static BattleSide getRandomSide() {
		return (Math.random() > 0.5) ? RED_ARMY : THIRD_EMPIRE;
	}

}
