package com.epam.stalingrad.domain;

public class Battleground {

    private final String name;

    public Battleground(final String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Battleground [name=").append(name).append("]");
        return builder.toString();
    }

}
