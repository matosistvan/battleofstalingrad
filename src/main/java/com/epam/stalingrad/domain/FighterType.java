package com.epam.stalingrad.domain;

public enum FighterType {

    ATTACKER, DEFENDER;

}
