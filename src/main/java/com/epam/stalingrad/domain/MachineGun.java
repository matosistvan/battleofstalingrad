package com.epam.stalingrad.domain;

public class MachineGun extends AbstractGun {

    public MachineGun(final int damage) {
        super(damage);
    }

    @Override
    public String shoot() {
        return "BULLETBULLETBULLETBULLETBULLET";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MachineGun [").append(super.toString()).append("]");
        return builder.toString();
    }

}
