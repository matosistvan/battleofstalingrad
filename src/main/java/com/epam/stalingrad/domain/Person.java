package com.epam.stalingrad.domain;

public abstract class Person {

    private final String name;

    public Person(final String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract String fight();
    
    public abstract String battleCry(LoudNess loudNess);
    
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Person [name=").append(name).append("]");
        return builder.toString();
    }

	public int getDogTagID() {
		return System.identityHashCode(this);
	}

}
