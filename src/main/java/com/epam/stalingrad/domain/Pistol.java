package com.epam.stalingrad.domain;

public class Pistol extends AbstractGun {

    public Pistol(final int damage) {
        super(damage);
    }

    @Override
    public String shoot() {
        return "bullet";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Pistol [").append(super.toString()).append("]");
        return builder.toString();
    }

}