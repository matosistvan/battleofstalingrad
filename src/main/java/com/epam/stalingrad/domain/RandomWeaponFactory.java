package com.epam.stalingrad.domain;

public abstract class RandomWeaponFactory {


    public Weapon produceWeapon() {
    	WeaponType weaponType = WeaponType.getRandomWeaponType();

        Weapon result = null;

        switch (weaponType) {
        case PISTOL:
            result = getPistol();
            break;
        case RIFLE:
            result = getRifle();
            break;
        case MACHINE_GUN:
            result = getMachineGun();
            break;
        default:
            throw new IllegalArgumentException("Not supported: " + weaponType);
        }

        return result;

    }


    public abstract Rifle getRifle();

    public abstract Pistol getPistol();

    public abstract MachineGun getMachineGun();

}
