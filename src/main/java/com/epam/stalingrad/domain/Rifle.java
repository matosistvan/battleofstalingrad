package com.epam.stalingrad.domain;

public class Rifle extends AbstractGun {

    public Rifle(final int damage) {
        super(damage);
    }

    @Override
    public String shoot() {
        return "BULLET";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Rifle [").append(super.toString()).append("]");
        return builder.toString();
    }

}
