package com.epam.stalingrad.domain;

public class Soldier extends Person {

    private Weapon weapon;

    public Soldier(final String name) {
        super(name);
    }

    @Override
    public String fight() {
        boolean flee = Math.random() < 0.5;
        if (flee) {
           return "I won't fight... off to fishing!";
        } else {
            return weapon == null ? "Puff-Puff (with your fists)" : weapon.shoot();

        }
    }

    @Override
    public String battleCry(LoudNess loudNess) {
        return "I will diii... placcss";
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(final Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Soldier [gun=").append(weapon).append("]");
        return builder.toString();
    }

}
