package com.epam.stalingrad.domain;

public interface Weapon {

    int getDamage();

    void setDamage(int damage);

    String shoot();

    int getUniqueId();

}
