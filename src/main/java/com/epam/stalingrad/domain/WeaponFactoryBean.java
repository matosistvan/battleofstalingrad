package com.epam.stalingrad.domain;

import org.springframework.beans.factory.FactoryBean;

public class WeaponFactoryBean implements FactoryBean<Weapon> {

    private final int damageRange;

    public WeaponFactoryBean(final int damageRange) {
        super();
        this.damageRange = damageRange;
    }

    @Override
    public Weapon getObject() throws Exception {

        boolean createRifle = Math.random() > 0.7;

        Weapon result = null;

        int damage = Double.valueOf(Math.random() * damageRange).intValue();

        if (createRifle) {
            result = new Rifle(damage);
        } else {
            result = new Pistol(damage);
        }

        return result;
    }

    @Override
    public Class<?> getObjectType() {
        return Weapon.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

}
