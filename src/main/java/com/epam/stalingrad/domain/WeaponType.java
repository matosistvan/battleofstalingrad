package com.epam.stalingrad.domain;

public enum WeaponType {

    PISTOL, RIFLE, MACHINE_GUN;

    public static WeaponType getRandomWeaponType() {

        double random = Math.random();

        return random >= 0.7 ? PISTOL : random > 0.5 ? RIFLE : MACHINE_GUN;
    }
}
